import http.server
import socketserver
import threading
import time

class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(bytes("OK", "utf-8"))

def start_server():
    with socketserver.TCPServer(("", 8000), MyHttpRequestHandler) as httpd:
        print("Server started at port 8000")
        httpd.serve_forever()

if __name__ == "__main__":
    server_thread = threading.Thread(target=start_server)
    server_thread.start()
