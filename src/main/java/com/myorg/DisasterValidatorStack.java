package com.myorg;

import org.jetbrains.annotations.NotNull;
import software.amazon.awscdk.*;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.services.appmesh.RouteProps;
import software.amazon.awscdk.services.ec2.*;
import software.amazon.awscdk.services.iam.*;
import software.amazon.awscdk.services.s3.Bucket;
import software.amazon.awscdk.services.s3.assets.Asset;
import software.amazon.awscdk.services.s3.assets.AssetProps;
import software.amazon.awscdk.services.s3.deployment.BucketDeployment;
import software.amazon.awscdk.services.s3.deployment.BucketDeploymentProps;
import software.amazon.awscdk.services.s3.deployment.ISource;
import software.amazon.awscdk.services.s3.deployment.Source;
import software.constructs.Construct;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
// import software.amazon.awscdk.Duration;
// import software.amazon.awscdk.services.sqs.Queue;

public class DisasterValidatorStack extends Stack {
    public DisasterValidatorStack(final Construct scope, final String id) throws IOException, InterruptedException {
        this(scope, id, null);
    }

    public DisasterValidatorStack(final Construct scope, final String id, final StackProps props) throws IOException, InterruptedException {
        super(scope, id, props);

        //set your key ID
        String keyId="key-0ec8a8df98a7dbc8b";
        String keyName="disasterkey";
        String instanceAMI = "ami-080e1f13689e07408"; //ubuntu 22.04
        InstanceType instanceType = InstanceType.of(InstanceClass.T2, InstanceSize.MEDIUM );

        //define the infrastructure size, (datacenter num, number of woker nodes)
        HashMap<Integer,Integer> DCXNode = new HashMap<>();
        DCXNode.put(0,2);
        DCXNode.put(1,2);
        DCXNode.put(2,2);


        // Create a VPC
        Vpc vpc = Vpc.Builder.create(this, "disaster_vpc")
                .defaultInstanceTenancy(DefaultInstanceTenancy.DEFAULT)
                .enableDnsSupport(true)
                .enableDnsHostnames(true)
                .subnetConfiguration(new ArrayList<>())
                .build();


        //create the Internet gateway
        CfnInternetGateway cfnInternetGateway = CfnInternetGateway.Builder.create(this, "disaster_gateway")
                .tags(List.of(CfnTag.builder()
                        .key("Name")
                        .value("disaster_IG")
                        .build()))
                .build();


        //create the attachment fom gateway to vpc
        CfnVPCGatewayAttachment cfnVPCGatewayAttachment = CfnVPCGatewayAttachment.Builder.create(this, "disaster_gateway_attachment")
                .vpcId(vpc.getVpcId())
                .internetGatewayId(cfnInternetGateway.getAttrInternetGatewayId())
                .build();


        ArrayList<PublicSubnet> subnets = createSubnets(4,vpc,cfnVPCGatewayAttachment);


        ISecurityGroup securityGroup = SecurityGroup.Builder.create(this, id + "-sg")
                .securityGroupName(id)
                .vpc(vpc)
                .build();
        securityGroup.addIngressRule(Peer.anyIpv4(), Port.allTraffic());
        securityGroup.addIngressRule(Peer.anyIpv6(), Port.allTraffic());

        IKeyPair iKeyPair= KeyPair.fromKeyPairName(this,keyId,keyName);

        // Create an S3 bucket
        Bucket bucket = Bucket.Builder.create(this, "disaster-test-bucket-23445")
                .bucketName("disaster-test-bucket-23445")
                .removalPolicy(RemovalPolicy.DESTROY)
                .autoDeleteObjects(true)
                .build();


        createCP("ControlPlane",vpc,instanceAMI,securityGroup,subnets.get(1),iKeyPair,instanceType,bucket);

        //uncomment to run getRT.sh
        for (Integer dc: DCXNode.keySet()) {
            Integer numberOfNodes = DCXNode.get(dc);
            for (int i = 0; i < numberOfNodes; i++) {
                createWN("DC_"+dc+"_Node_"+i,vpc,instanceAMI,securityGroup,subnets.get(dc),iKeyPair,instanceType,bucket,dc);
            }
        }

        //uncomment to run getRestartTime.sh
//        createWN("WN1",vpc,instanceAMI,securityGroup,subnets.get(1),iKeyPair,instanceType,bucket,1);
//        createWN("WN2",vpc,instanceAMI,securityGroup,subnets.get(2),iKeyPair,instanceType,bucket,2);

    }

    public ArrayList<PublicSubnet> createSubnets(int numberOfZones, Vpc vpc, CfnVPCGatewayAttachment cfnVPCGatewayAttachment){
        List<String> availabilityZones = getAvailabilityZones();

        ArrayList<PublicSubnet> createtSubnets = new ArrayList<>();

        for (int i = 0; i < numberOfZones; i++) {
            PublicSubnet publicSubnet = new PublicSubnet(this,"publicsubnet_"+i,PublicSubnetProps
                    .builder()
                    .availabilityZone(availabilityZones.get(i))
                    .cidrBlock("10.0."+i+".0/24")
                    .vpcId(vpc.getVpcId())
                    .mapPublicIpOnLaunch(true)
                    .build()
            );
            createtSubnets.add(publicSubnet);

            CfnRoute cfnRoute = CfnRoute.Builder.create(this, "disaster_route_"+i)
                    .routeTableId(publicSubnet.getRouteTable().getRouteTableId())
                    .gatewayId(cfnVPCGatewayAttachment.getInternetGatewayId())
                    .destinationCidrBlock("0.0.0.0/0")
                    .build();
        }
        return createtSubnets;
    }


    public Instance createCP(String name, Vpc vpc, String instanceAMI,ISecurityGroup securityGroup,Subnet subnet,IKeyPair iKeyPair,InstanceType instanceType,Bucket bucket) throws IOException {

        String controlPlaneData = Files.readString(Paths.get("./src/scripts/controlPlaneInstall.sh"), Charset.defaultCharset());
        String kubDeployment = Files.readString(Paths.get("./src/scripts/kubernetes/kub_deployment.yaml"), Charset.defaultCharset());
        String kubService = Files.readString(Paths.get("./src/scripts/kubernetes/kub_service.yaml"), Charset.defaultCharset());
        String afterInstall = Files.readString(Paths.get("./src/scripts/afterInstall.sh"), Charset.defaultCharset());

        String userDataString = "echo '"+controlPlaneData+"' > /home/ubuntu/controlPlaneInstall.sh\n" +
                "echo '"+kubDeployment+"' > /home/ubuntu/kub_deployment.yaml\n" +
                "echo '"+kubService+"' > /home/ubuntu/kub_service.yaml\n" +
                "echo '"+afterInstall+"' > /home/ubuntu/afterInstall.sh\n" +
                "apt-get update -y\n" +
                "chmod +x /home/ubuntu/controlPlaneInstall.sh\n" +
                "/home/ubuntu/controlPlaneInstall.sh\n" +
                "chmod +x /home/ubuntu/afterInstall.sh\n" +
                "/home/ubuntu/afterInstall.sh "+bucket.getBucketName()+"\n";

        Instance instance=createEc2Instance(name,vpc,instanceAMI,securityGroup,subnet,iKeyPair,userDataString,instanceType);

        bucket.grantReadWrite(instance);

        PolicyStatement policyStatement = PolicyStatement.Builder.create()
                .effect(Effect.ALLOW)
                .actions(Collections.singletonList("ec2:*"))
                .resources(List.of("*"))
                .build();

        instance.getRole().addToPrincipalPolicy(policyStatement);

        return instance;
    }

    public Instance createWN(String name, Vpc vpc, String instanceAMI,ISecurityGroup securityGroup,Subnet subnet,IKeyPair iKeyPair,InstanceType instanceType,Bucket bucket,Integer dcNum) throws IOException {

        String workerNodeInstall = Files.readString(Paths.get("./src/scripts/workerNodeInstall.sh"), Charset.defaultCharset());

        String userDataString = "echo '"+workerNodeInstall+"' > /home/ubuntu/workerNodeInstall.sh\n" +
                "apt-get update -y\n" +
                "chmod +x /home/ubuntu/workerNodeInstall.sh\n" +
                "/home/ubuntu/workerNodeInstall.sh "+bucket.getBucketName()+"\n";

        Instance instance=createEc2Instance(name,vpc,instanceAMI,securityGroup,subnet,iKeyPair,userDataString,instanceType);

        Tags.of(instance).add("DC", ""+dcNum);

        bucket.grantReadWrite(instance);

        return instance;
    }

    public Instance createEc2Instance(String name, Vpc vpc, String instanceAMI,ISecurityGroup securityGroup,Subnet subnet,IKeyPair iKeyPair,String userDataString,InstanceType instanceType) throws IOException {

        Map<String, String> armUbuntuAMIs = new HashMap<>();
        armUbuntuAMIs.put("us-east-1", instanceAMI);

        final IMachineImage armUbuntuMachineImage = MachineImage.genericLinux(armUbuntuAMIs);

        UserData userData = UserData.forLinux();
        userData.addCommands(userDataString);

        Instance engineEC2Instance = Instance.Builder.create(this,  name)
                .instanceName(name)
                .machineImage(armUbuntuMachineImage)
                .securityGroup(securityGroup)
                .vpcSubnets(SubnetSelection.builder().subnets(new ArrayList<>(List.of(new Subnet[]{subnet}))).build())
                .instanceType(instanceType)
                .keyPair(iKeyPair)
                .vpc(vpc)
                .userData(userData)
                .blockDevices(
                        List.of(
                                BlockDevice.builder()
                                        .deviceName("/dev/sda1") // Root volume device name
                                        .volume(BlockDeviceVolume.ebs(50))
                                        .build()
                        )
                )
                .build();


        CfnOutput.Builder.create(this, "VpcIPOutput"+name)
                .value(engineEC2Instance.getInstance().getAttrPublicIp())
                .build();

        return engineEC2Instance;

    }




}
