package com.myorg;

import software.amazon.awscdk.App;
import software.amazon.awscdk.Environment;
import software.amazon.awscdk.StackProps;

import java.io.IOException;
import java.util.Arrays;

public class DisasterValidatorApp {
    public static void main(final String[] args) throws IOException, InterruptedException {
        App app = new App();

        new DisasterValidatorStack(app, "DisasterValidatorStack", StackProps.builder()
                .env(Environment.builder()
                        .account(System.getenv("CDK_DEFAULT_ACCOUNT"))
                        .region(System.getenv("CDK_DEFAULT_REGION"))
                        .build())
                .build());

        app.synth();
    }
}

